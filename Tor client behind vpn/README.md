# How to run a container behind a vpn

## References
- [gluetun (Lightweight Swiss-knife VPN client to connect to several VPN providers)](https://hub.docker.com/r/qmcgaw/gluetun)
- [gluetun environment variables](https://github.com/qdm12/gluetun/wiki/Environment-variables)
- [Connect other containers to gluetun](https://github.com/qdm12/gluetun/wiki/Connect-to-gluetun#Access-ports-of-containers-connected-to-Gluetun)
- [Using Docker Environment Variales](https://www.techrepublic.com/article/how-to-use-docker-env-file/#:~:text=These%20value%20pairs%20(within%20the.env%20file)%20are%20used,and%20they%20must%20be%20edited%20directly%20if%20changed.)

# Getting Started

- Create a ".env" file in the root directory with your `docker-compose.yml`
    - Store your secrets here in a `KEY=VALUE` format with one pair per line
    - Reference these in your docker-compose.yml file using `"${KEY}"` including the quotes and it will be replaced with the value
- In my `docker-compose.yml` I have the environment variables set to use the PIA vpn service. Please reference the [gluetun environment variables](https://github.com/qdm12/gluetun/wiki/Environment-variables) if you use another vpn service provider to see what options to set.
- After the gluetun vpn container starts docker-compose will start all other containers that uses it for their network mode.
- I used a torrent client as an example but you could put an httpd container or any other. 
- To access the ports needed on the containers behind the vpn, be sure to expose those ports under the gluten settings as shown then in your browser or whatever client you would normally use to connect go to `localhost:port#` and you should be connected. 
    - In the case of my example going to `localhost:8112` will give me the login page to access to Web Management page for the deluge torrent client.